//
//  FactoryOpenViewController.m
//  TransactionViewController
//
//  Created by Andre Vieira on 16/07/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import "FactoryOpenViewController.h"

@implementation FactoryOpenViewController


// this method is one fectory with return UIVewController
+(UIViewController *)initFromController:(NSString *)vcName
                           inStoryboard:(NSString *)sbName
                          withAnimation:(UIModalTransitionStyle *)modalTransitionStyle{
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:sbName bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:vcName];
    vc.modalTransitionStyle = modalTransitionStyle;
    return vc;
}

@end
