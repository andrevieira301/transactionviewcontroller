//
//  NewViewController.m
//  TransactionViewController
//
//  Created by Andre Vieira on 16/07/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import "NewViewController.h"

@interface NewViewController ()

@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation NewViewController


-(void)viewDidLoad{
    self.label.text = self.textLabel;
}

@end
