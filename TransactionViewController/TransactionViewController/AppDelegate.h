//
//  AppDelegate.h
//  TransactionViewController
//
//  Created by Andre Vieira on 16/07/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

