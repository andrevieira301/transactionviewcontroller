//
//  FactoryOpenViewController.h
//  TransactionViewController
//
//  Created by Andre Vieira on 16/07/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FactoryOpenViewController : UIViewController

+(UIViewController *)initFromController:(NSString *)vcName
                           inStoryboard:(NSString *)sbName
                          withAnimation:(UIModalTransitionStyle *)modalTransitionStyle;

@end
