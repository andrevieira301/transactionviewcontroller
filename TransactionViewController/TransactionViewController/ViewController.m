//
//  ViewController.m
//  TransactionViewController
//
//  Created by Andre Vieira on 16/07/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)OpenNewViewController:(id)sender{
    NewViewController *vc = [FactoryOpenViewController initFromController:@"NewViewController" inStoryboard:@"Main" withAnimation:UIModalTransitionStylePartialCurl];
    
    //set property NewViewController
    vc.textLabel = @"so passes a parameter";
    
    [self presentViewController:vc animated:YES completion:nil];
}

@end
